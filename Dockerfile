FROM python:3.6.9

LABEL version="0.0.1"

ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir /app
RUN mkdir /nltk_data
WORKDIR /app

COPY Pip* /app/

RUN pip install --upgrade pip && \
    pip install pipenv && \
    pipenv install --dev --system --deploy --ignore-pipfile

EXPOSE 8080

CMD waitress-serve --call ailearning:create_app