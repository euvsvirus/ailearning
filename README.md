## FOR DEVS

## RUN IT!!!
First of all make sure you have python >= 3.6 and docker installed. After cloning the project, run:

    cd <path-to-project>
    docker build -t <image-name> .
    docker run --name <container-name> -p 8090:8090 -v  <absolute-path-to-project>:/app <image-name>

## The problem your we solve
Due to Covid spread, the social distancing became our daily routine, the laptop our school desk. If lessons moved from classroom to elearning platform, not without issues, **exams** still remain a big question point for many universities and schools.
The preparation and execution of a simple exams became an enormous obstacle: the distance from classmates bring to a lack of feedback for each student about his/her own preparation, during the exam students tend to be cheating than usual, there is a lack of scan tools for written exams, time-consuming oral exams because each one requires at least one witness for validation.
## Our Solution: _aiLearning_
**_aiLearning_ is an AI-based virtual assistant developed with the aim to simplify remote examinations.**
Since social distancing can’t ruin the magic of a conversation, _aiLearning_ provides the possibility to **take an exam by speaking with a bot**. Each teacher can share simultaneously all questions with all students using a single interface and without needed any witness. All students can interact with the **bot** providing to it the best answer for each exam question. Checks on student voice can demonstrate that the who is the real speaker, unmasking cheating students.
**Is that all? Absolutely no.**
_aiLearning_ supports teachers in managing the most time consuming activity: **exams corrections**. Thanks to **Machine Learning** algorithms, _aiLearning_ analyzes students answers providing them feedbacks on their preparation.
In this way teachers have more time to dedicate to keep in contact with students for proceeding with the learning path, but on the other side students can use _aiLearning_ as a **learning companion** for a self-assessment step before the final exam.
_aiLearning_ is not another technology that must be added to the huge number of platforms currently used for e-learning, but it’s can be easily integrated through API with any platform used by universities or schools.
## What we’ve done during the weekend
_aiLearning_ idea is born on 24th April during a virtual meeting among friends currently located in three different cities of two countries (Italy and France).
During the weekend _aiLearning_ has taken its shape, becoming **a real virtual assistant** integrated with Slack, with a completed design of the project: a detailed business plan, costs definition, development efforts for finalization, interface design and go-to-market plan.
Data scientists of _aiLearning_ team developed the AI algorithm, data engineers integrated it with Slack, the user experience e user interface designer defined the branding and a user-friendly service interface, economists define the business model and got-to-market approach and finally the legal specialist checked all privacy and security aspects related to the solution.
#### Just some details on weekend job of _aiLearning_ team.
The AI algorithm, developed with Python code, currently manages the following steps:
- **Speech to text** conversion in order to turn the voice command input into textual data.
- **Natural Language Processing (NLP) and Understanding (NLU)** step has been developed for text mining solution in order to analyze students answers.
- **Machine Learning** algorithm define the level of correctness of each answer thanks to similarity models based on different weighting factor used for information retrieval solution (such as tf-idf).
We developed a prototype that simulates an Art exams with questions on three different paintings: “_La Gioconda_”, “_The Kiss_”, “_Guernica_”.
Each student answer is compared with the teacher expectation, providing a feedback to the student on his/her level of preparation.
Just to provide some examples:
```
Gioconda is a painting of Michelangelo.
```
In this case, _aiLearning_ kindly invites the student to open the Art book for the first time.
```
Gioconda, called also Mona Lisa is a painting of the italian artist Leonardo da Vinci
```
aiLearning understands that the student at least opened the book, but it is not still enough.
```
The Gioconda, called also Mona Lisa is a painting of the italian artist Leonardo da Vinci, \
it's considered an archetypal masterpiece during the Renaissance. \
It's a half-length portrait of a noblewoman Lisa Gherardini, wife of Francesco del Giocondo
```
Finally _aiLearning_ received a complete and good answer: the student made a good job!
Due to the fact that the weekend has only 48 hours, we developed the **speech-to-text** step within the code, but the integration with _Slack_ is done with a textual input: the student, in this case, provides the answer by chatting with the virtual assistant. The integration has been done by using Slack API.
During the weekend we also designed the mock-up of the web application: a **user-friendly interface** that the teacher can use in order to share documents or links to be used by students for exams preparation. The web application will provide a set of dashboards that the teacher can use in order to see exams results, to re-listen students answers or to read each exam textual transcription. Moreover, this platform could be integrated with university (or school) platform in order to automatically register and publish final exams scores.
From a business point of view, during the weekend we defined **a complete business model**. The distribution of the solution is supposed to be on-premise, with a proprietary code licencing. The revenue stream for the purchase and usage of _aiLearning_ solution is linked to the number of subscriptions or it can be defined with a usage-based approach. The interaction model is one-to-many: a student can interact by himself for an evaluation pre-assessment, but the teacher can interact simultaneously with all exam attendees.
## The solution’s impact to the crisis
Since social distancing can’t ruin the magic of a conversation, _aiLearning_ provides the possibility to take an exam by speaking with a bot. Each teacher can share **simultaneously** all questions with all students using a single interface and without needed any **witness**. All students can interact with the bot providing to it the best answer for each exam question. Checks on student voice can demonstrate that the who is the real speaker, unmasking cheating students.
Since remote education requires more time with respect to the located learning, _aiLearning_ is aimed to free teachers **time supporting** them with the activities related to exams correction.  This time can be better involved to support students with their learning path.
## The necessities in order to continue the project
We estimate that in order to close the MVP phase we need around 170.000 €.
The MVP will include a test with two universities (or schools) in order to get feedbacks and upgrades from final users for achieving the final version of the solution.
Scaling-up improvements will require additive efforts.
## The value of the solution after the crisis
The time consuming aspect of oral exams is not a problem only related to Covid situation. In many universities or school, there is often a short time-boxed window for exams and so many students to examine.
_aiLearning_ can be used as solution of this issue also after the crisis providing a valid alternative for simultaneous and time-saving exams.
Moreover, _aiLearning_ can continue to be a good learning companion also after our home doors will be opened.
## The URL to the prototype [Github, Website,...]
A draft of the code is available on GitLab at: [https://gitlab.com/euvsvirus/aiLearning](https://gitlab.com/euvsvirus/ailearning)
Using the following credential it is possible to login on the workspace Slack [https://ailearning-talk.slack.com](https://ailearning-talk.slack.com) in order to try to answer to some questions related to “_La Gioconda_”, “_The Kiss_”, “_Guernica_”:
- email: team.ailearning@outlook.com
- psw: EUvsVirus
Please use the channel **#examination** in order to interact with **aiLearning** app. In order to start the exam and ask for new question, write “next” as Slack message. The released solution works only with one user per time.
The code related to the Machine Learning algorithm has not fully provided since it is still ongoing. We currently manage the evaluation of the answer content, but we still are working on other evaluation criteria, such as synthesis skills or expression ability.
For further information, or for any doubt/question/curiosity you can contact us at: **team@ai-learning.eu**