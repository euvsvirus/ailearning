class BaseConfig():
   
   TESTING = False
   DEBUG = False
   HOSTNAME = "<hostname>:<port>"
   CURRENT_QUESTION = None

class DevConfig(BaseConfig):

   ENV = 'development'
   SERVICE_NAME = 'ailearning-dev'
      
   DEBUG = True
   TESTING = True

   SLACK_TOKEN = '<your-bot-token-here>'
   SLACK_API_URL = 'https://slack.com/api/chat.postMessage'


