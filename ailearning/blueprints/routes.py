from ailearning.resources.messages import MessagesResource

# Link endpoint to a specific resource
def initialize_routes(api):

    api.add_resource(MessagesResource, '/messages')
