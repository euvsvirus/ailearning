class AIAnalyser():

    def __init__(self, minimum=0, maximum=1):

        self._minimum = minimum
        self._maximum = maximum

    def analyse(self, question_id, answer):

        ### YOU SHALL NOT PASS !!! ###

        # The code related to the Machine Learning algorithm has not been fully provided 
        # since it is still ongoing. We currently manage the evaluation of the answer content, 
        # but we are still working on other evaluation criteria, such as synthesis skills 
        # or expression ability.

        # Moreover, we realized that the solution we implemented reached a level of 
        # complexity and accuracy which we did not expect. For these reason, we would
        # like to keep the code proprietary until the team will take an a proper decision
        # about the project

        # For further information, or for any doubt/question/curiosity 
        # you can contact us at: team@ai-learning.eu
        # We would be happy to exchange concerns/ideas around it!
    
        return self._teacher_answer(0.5)
    
    def _teacher_answer(self, vote):

        if vote < 0.3:
            response = "Are you sure that you studied this topic?"
        elif vote < 0.5:
            response = "It's ok, but I think that you can do better."
        elif vote < 0.7:
            response = "Well done."
        else:
            response = "You are perfect!"

        return response
    
    @property
    def minimum(self) -> int:
        return self._minimum
    
    @minimum.setter
    def minimum(self, minimum: int):
        self._minimum = minimum
    
    @property
    def maximum(self) -> int:
        return self._maximum
    
    @maximum.setter
    def maximum(self, maximum: int):
        self._maximum = maximum
    
    
