def get_uestion_as_image(text, url, caption = None):
    data = {}
    data['type'] = "image"
    data['title'] = get_text(text)
    data['image_url'] = url
    data['alt_text'] = caption
    return data

def get_text(text) :
    data = {}
    data['text'] = text
    data['type'] = "plain_text"
    return data

def get_accessory(imageUrl, caption):
    data = {}
    data['type'] = "image"
    data['image_url'] = imageUrl
    data['alt_text'] = caption
    return data

