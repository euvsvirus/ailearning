from flask import current_app

hostname = current_app.config.get("HOSTNAME")

questions = [
    {
        "id": 1,
        "text" : "What do you know about the La Gioconda painting?",
        "url" : hostname + "/static/images/gioconda.jpg",
        "caption" : " "
    },
    {
        "id": 2,
        "text" : "What do you know about the Guernica painting?",
        "url" : hostname + "/static/images/guernica.jpg",
        "caption" : " "
    },
    {
        "id": 3,
        "text" : "What do you know about The Kiss painting?",
        "url" : hostname + "/static/images/thekiss.jpg",
        "caption" : " "
    }
]