import requests
import random
from ailearning.test_set.questions import questions

from ailearning.lib import utils as u
from urllib import parse
from flask import make_response, request, current_app
from flask_restful import Resource

from ailearning import logger
from ailearning.lib import ai_analyser as ai


class MessagesResource(Resource):

    COMMANDS = ["start", "next"]
    DEFAULT_MESSAGE = "I don't know what to do. May try with start :)"

    def get(self):
        
        # Just an example
        return make_response({
            "channel": "ailearning",
            "user": "zlatanibra",
            "message": "The Gioconda is a painting made by Leonardo"
        })
    
    def post(self):

        req_json = request.get_json()
        logger.info('POST messages request received: {}'.format(req_json))

        # First andshake with slack app
        if req_json.get('type') == 'url_verification':
            make_response({'challenge': req_json.get('challenge')})

        data = {
            'token': current_app.config.get('SLACK_TOKEN'),
            'channel': '<your-channel-here>'
        }

        analyser = ai.AIAnalyser()
        question = None
        blocks = []

        if not req_json.get('event', {}).get('bot_id'): # Let's not answer to myself

            if req_json.get('event', {}).get('text', "").lower() in self.COMMANDS:
                
                # Let's build a question
                question = questions[(random.randint(0, len(questions) - 1))]
                blocks.append(u.get_uestion_as_image(question.get("text"), \
                     question.get("url"), question.get("caption")))
                data.update({'blocks': blocks})

                # Not that elegant because of time constraints, I would use db like Redis
                # to store the context or some third-party service like DialogFlow
                current_app.config["CURRENT_QUESTION"] = question.get("id")

                _ = requests.post(current_app.config.get('SLACK_API_URL'), data=data)

            elif not current_app.config["CURRENT_QUESTION"]:

                data.update({'text': self.DEFAULT_MESSAGE})

            else: 

                answer = [req_json.get('event', {}).get('text')]
                
                # Now the best part :)
                evaluation = analyser.analyse(current_app.config["CURRENT_QUESTION"], answer)
                data.update({'text': evaluation})

                # Clear the context
                current_app.config["CURRENT_QUESTION"] = None

            response = requests.post(current_app.config.get('SLACK_API_URL'), \
                 data=parse.urlencode(data).encode(), \
                 headers={"content-type": "application/x-www-form-urlencoded"})

            logger.debug(response.text)

        return make_response({})